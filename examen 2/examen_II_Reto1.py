def dibujo_rombo(valor):
  resultado = [" " * (valor - i) + "*" * (i + i - 1) for i in range(1, valor + 1)]

  return "\n".join(resultado + list(reversed(resultado[:-1])))

# Llamada al método y entrada de datos       
entrada_numero = int(input("Introduzca un número: "))

print(dibujo_rombo(entrada_numero))